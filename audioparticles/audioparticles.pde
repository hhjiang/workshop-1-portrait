/* CS/FINE 383: Assignment 1
 *  Helga Jiang (20578811)
 * 
 * 
 *    Face tracking + fullscreen code based on in-class demos.
 *    Minim components based on minim library examples: http://code.compartmental.net/minim/
 *    Particle class adapted from Daniel Shiffman's Multiple Particle System: https://processing.org/examples/multipleparticlesystems.html
 */

import processing.video.*;
boolean fullScreen;
float fullScreenScale;
// scale factor to downsample frame for processing 
float screenScale = 0.5;

import gab.opencv.*;

// to get Java Rectangle type
import java.awt.*; 

import ddf.minim.*;
import ddf.minim.analysis.*;
 
Minim minim;
AudioInput liveInput; //live mic input
BeatDetect beat; //beat detection

Capture cam; //grabs frame and passes it into the openCV object
OpenCV opencv; //way to access the opencv methods

// scale factor to downsample frame for processing 
float scale = 0.5; //affects frame rate & the pixelation

// image to display
PImage output;

// array of bounding boxes for face
Rectangle[] faces;

//particles
final static ArrayList<Particle> particles = new ArrayList();   //Initialize the arraylist of particles
color[] colours = new int[100]; //list of colours

//debugging flag
boolean debug = false;

void setup() {
  // uncomment or comment these four lines to switch between fullscreen
  fullScreen(); 
  fullScreen = true;
  //size(640, 480); 
  //fullScreen = false;
  // assumes same aspect ratio, otherwise pick if you want w or h to define size
  fullScreenScale = width / float(640);

  colours[0] = color(random(255), random(255), random(255)); //add initial colour to list of colours
  
  minim = new Minim(this); //pass this to Minim so that it can load files from the data directory
  liveInput = minim.getLineIn(); // use the getLineIn method of the Minim object to get an AudioInput

  // want video frame and opencv proccessing to same size
  cam = new Capture(this, int(640 * scale), int(480 * scale));

  opencv = new OpenCV(this, cam.width, cam.height);
  opencv.loadCascade(OpenCV.CASCADE_FRONTALFACE);  //can detect other objects too, like mouth

  cam.start();

  // init to empty image
  output = new PImage(cam.width, cam.height); //initialize output image
}


void draw() {
  // clear background to black
  background(0);
  
  if (cam.available() == true) {
    cam.read();

    // load frame into OpenCV 
    opencv.loadImage(cam);

    // it's often useful to mirror image to make interaction easier
    // 1 = mirror image along x
    // 0 = mirror image along y
    // -1 = mirror x and y
    opencv.flip(1);

    faces = opencv.detect();

    // switch to RGB mode before we grab the image to display
    opencv.useColor(RGB);
    output = opencv.getSnapshot(); //takes current version of image inside openCV
  }
  fill(0, 0, 0, .5);


  // draw the image and graphics at full scale resolution
  pushMatrix();
  if (fullScreen) { 
    scale(fullScreenScale, fullScreenScale);
  } 
  
  // draw the image
  pushMatrix();
  scale(1 / scale);
  tint(255, 128); // partially transparent image
  image(output, 0, 0 );
  popMatrix();
  
  stroke(255,255,255);

  // draw face tracking debug
  if (faces != null) {
    for (int i = 0; i < faces.length; i++) {
      
      // scale the tracked faces to canvas size
      float s = 1 / scale;
      int facex = int(faces[i].x * s);
      int facey = int(faces[i].y * s);
      int facew = int(faces[i].width * s);
      int faceh = int(faces[i].height * s);
      

      if (colours.length <= i) { //generate a new colour if there is a new face
        println (colours.length);
        colours[i] = color(random(255), random(255), random(255));
      }

      //if (i == 0) {
      //  c = color(random(255), random(255), random(255));
      //}

      particles.add(new Particle(new PVector(facex + facew/2, facey), colours[i]));
      for (int j = particles.size()-1; j >= 0; j--) {
        Particle p = particles.get(j);
        p.run();
        if (p.isDead()) {
          particles.remove(j);
        }
      }
      
    }
  }
  else { // if there are no faces, clear the list of colours to create new ones
    for(color c : colours){
      // c = null;
    }
  }
  popMatrix();
  fill(255, 0, 0);
  text(nfc(frameRate, 1), 20, 20);
}

// A simple Particle class, adapted from Daniel Shiffman's Multiple Particle System
//     https://processing.org/examples/multipleparticlesystems.html
class Particle {
  PVector position;
  PVector velocity;
  PVector acceleration;
  float lifespan;
  color colour;

  Particle(PVector l, color c) {
    acceleration = new PVector(0, 0.05);
    velocity = new PVector(random(-1, 1), random(-2, 0));
    position = l.copy();
    lifespan = 305.0;
    colour = c;
  }

  void run() {
    update();
    display();
  }

  // Method to update position
  void update() {
    velocity.add(acceleration);
    position.add(velocity);
    lifespan -= 5.0;
  }

  // Method to display
  void display() {
    stroke(colour, lifespan);
    fill(colour, lifespan);
    ellipse(position.x, position.y, liveInput.left.level()*250, liveInput.left.level()*250);
  }

  // Determines if the particle should be removed
  boolean isDead() {
    return (lifespan < 0.0);
  }
}
 