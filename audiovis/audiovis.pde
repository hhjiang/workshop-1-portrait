/* CS/FINE 383: Assignment 1
*  Helga Jiang (20578811)
*/

import ddf.minim.*;
import ddf.minim.analysis.*;
 
Minim minim;
AudioPlayer song; //load sound file into song
//AudioMetaData meta; //An object filled with metadata about a file, such as ID3 tags.
BeatDetect beat; //beat detection

float eRadius;

void setup(){
  size(500,500);

  //pass this to Minim so that it can load files from the data directory
  minim = new Minim(this);
  song = minim.loadFile("song.mp3"); //load in the song file
  //meta = player.getMetaData();
  beat = new BeatDetect(); //http://code.compartmental.net/minim/beatdetect_class_beatdetect.html
  song.loop(); 
  
  ellipseMode(RADIUS);
  eRadius = 20;
}
 
void draw(){
  background(0);
  stroke(255);
  //noFill();
  
  
  // draw the waveforms
  // the values returned by left.get() and right.get() will be between -1 and 1,
  // so we need to scale them up to see the waveform
  // note that if the file is MONO, left.get() and right.get() will return the same value
  // adapted from Minim doc examples: http://code.compartmental.net/minim/audioplayer_field_mix.html
  for(int i = 0; i < song.bufferSize() - 1; i++)
  {
    float x1 = map( i, 0, song.bufferSize(), 0, width );
    float x2 = map( i+1, 0, song.bufferSize(), 0, width );
    line( x1, 50 + song.left.get(i)*50, x2, 50 + song.left.get(i+1)*50 );
    line( x1, 150 + song.right.get(i)*50, x2, 150 + song.right.get(i+1)*50 );
  }
  
  // draw a line to show where in the song playback is currently located
  float posx = map(song.position(), 0, song.length(), 0, width);
  stroke(0,200,0);
  line(posx, 0, posx, height);
  
  if ( song.isPlaying() )
  {
    text("Press any key to pause playback.", 10, 20 );
  }
  else
  {
    text("Press any key to start playback.", 10, 20 );
  }
  
  //beat visual
  beat.detect(song.mix);
  float a = map(eRadius, 20, 80, 60, 255);
  fill(60, 255, 0, a);
  if ( beat.isOnset() ) eRadius = 80;
  ellipse(mouseX, mouseY, eRadius, eRadius);
  eRadius *= 0.95;
  //if ( eRadius < 20 ) eRadius = 20;
}

void keyPressed()
{
  if ( song.isPlaying() )
  {
    song.pause();
  }
  // if the player is at the end of the file,
  // we have to rewind it before telling it to play again
  else if ( song.position() == song.length() )
  {
    song.rewind();
    song.play();
  }
  else
  {
    song.play();
  }
}
 