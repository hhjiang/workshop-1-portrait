/* Helga Jiang (20578811)
 * CS 383: Assignment 2
 * Dandelion Thought Machine
 *
 *
 */

Gui gui;

import de.looksgood.ani.*;
import de.looksgood.ani.easing.*;

import ddf.minim.*;
import ddf.minim.analysis.*;
 
Minim minim;
AudioInput liveInput; //live mic input

// global variables for agents
float shapeScale = 1;
float shapeAngle = 0; 
int shapeNum = 1;
float maxDist;
float distScale = 1;
// number of tiles across width of grid 
int tiles; 
int agentsCount = 80;
// list of agents
ArrayList<Agent> agents;

void setup() {
  size(600, 600);
  //fullScreen();

  minim = new Minim(this); //pass this to Minim so that it can load files from the data directory
  liveInput = minim.getLineIn(); // use the getLineIn method of the Minim object to get an AudioInput


  // you have to call always Ani.init() first!
  Ani.init(this);

  // setup the simple Gui
  gui = new Gui(this);

  // add parameters to the Gui
  gui.addSlider("shape", 1, 9); 
  
  maxDist = sqrt(sq(width) + sq(height));

  createAgents(width/2, height/2);
}

void createAgents(int x, int y) {
  int time = millis();
  // load the SVG shape 
  PShape shape = loadShape("petal.svg");
  //shape.disableStyle();
  
  agents = new ArrayList<Agent>();

  // step size between grid centres
  //float step = width / tiles;
  // the length of the agents line (diagonal line of tile)
  //float length = sqrt(step * step + step * step);
  int angle;
  float maxScale;
  agents = new ArrayList<Agent>();
  int initPetals = 15; //original starting petals (add from the back) so there is an even circle
  for (int i = 0; i < agentsCount; i++) {
    if (i<initPetals) {
      angle = i*(360/initPetals);
      maxScale = random(1.15,1.2);
    }
    else {
      angle = int(random(i*30, i*60));
      maxScale = random(1/(i+1),1.2);
    }
    color colour = color(255, random(230, 250), random(50,80));
    Agent a = new Agent(x, y, angle, maxScale, colour, shape, time);
    agents.add(a);
  }
}

void draw() {
  background(0);

  // draw all the agents
  for (Agent a : agents) {
    a.update();
    a.draw();
  }

  // draw Gui last
  gui.draw();
}

// enables shortcut keys for the Gui
// 'm' or 'M' to show menu, 'm' to partially hide, 'M' to fully hide
void keyPressed() {
  gui.keyPressed();
}