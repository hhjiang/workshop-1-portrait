
class Agent {

  // local shape transforms
  float scale = 1;
  float curScale = 0.1;
  boolean growing = true;
  String state = "petal";
  

  // location of agent centre and shape to display
  float x;
  float y;
  float angle;
  float maxScale; //how large the petal will eventually grow to
  float growVelocity; //how fast the petal grows
  color colour;
  PShape shape;
  int time; //the time at which the agent was born
  
  // create the agent
  Agent(float _x, float _y, float _angle, float _maxScale, color _colour, PShape _shape, int _time) {
    x = _x;
    y = _y;
    angle = _angle;
    maxScale = _maxScale;
    shape = _shape;
    colour = _colour;
    time = _time;
  }

  void update() {
    //float nextAngle = angle + random(-3,3);
    //Ani.to(this, 1, "angle", nextAngle, Ani.QUAD_OUT);
    
    int m = millis();
    int petalTime = 15; //number of seconds before petals start closing
    int seedTime = petalTime + 10; //number of seconds before seed state starts
    int blowTime = seedTime + 10; //number of seconds before seeds start propogating
    
    angle += random(-200*liveInput.left.level(),200*liveInput.left.level()); //makes the petals 'move'
    println(liveInput.left.level());
    
    if ((curScale < maxScale) && growing){
      Ani.to(this, random(6,8), "curScale", maxScale+1, Ani.QUAD_OUT);
      
    }
    if (m-time >= petalTime * 1000 && state=="petal"){ //close the flower
      Ani.to(this, random(5,7), "curScale", 0, Ani.QUAD_OUT);
      growing = false;
    }
    if (m-time >= seedTime * 1000){ //produce seeds
      state = "seed";
      shape = loadShape("seed.svg"); //change to seed
      growing = true;
    }
    if (m-time >= blowTime && state == "seed"){ //seeds start propogating
    }
  }

  void draw() {
    pushMatrix();
    translate(x, y);
    rotate(radians(shapeAngle + angle));
    noStroke();
    noFill();    
    shapeMode(CENTER);
   
    //fill(colour);
    float s = curScale*20;
    shape(shape, 0, 0, s, s);
    popMatrix();
  }
}