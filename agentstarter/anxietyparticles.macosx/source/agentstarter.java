import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import controlP5.*; 
import java.util.Calendar; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class agentstarter extends PApplet {

/*
 * Starter code for creating a generative drawing using agents
 * 
 *
 */

Gui gui;

// list of agents
ArrayList<Agent> agents;

int agentsCount;

// add your agent parameters here
float maxStepSize = 5;

float opacity = 20;
float maxWeight = 50;
//base colour
float baseHue = 200;

public void setup() {
  //size(800, 600);
  

  agentsCount = height / 3;

  // setup the simple Gui
  gui = new Gui(this);

  gui.addSlider("agentsCount", 10, height);
  gui.addSlider("maxStepSize", 0, 100);
  gui.addSlider("opacity", 0, 255);
  gui.addSlider("maxWeight", 0, 200);
  gui.addSlider("baseHue", 0, 360);
  colorMode(HSB, 360, 100, 100, 100);
  createAgents();
}

public void createAgents() {
  // create Agents in a centred starting grid
  agents = new ArrayList<Agent>();
  for (int i = 0; i < agentsCount; i++) {
    Agent a = new Agent();
    agents.add(a);
  }
}

public void draw() {
  background(360);
  
  // update all agents
  // draw all the agents
  for (Agent a : agents) {
    a.update();
  }

  // draw all the agents
  for (Agent a : agents) {
    a.draw();
  }

  // draw Gui last
  gui.draw();

  // interactively adjust agent parameters
  //param = map(mouseX, 0, width, 0, 10);
}

// enables shortcut keys for the Gui
// 'm' or 'M' to show menu, 'm' to partially hide, 'M' to fully hide
public void keyPressed() {
  gui.keyPressed();

  // space to reset all agents
  if (key == ' ') {
    createAgents();
  }
}

// call back from Gui
public void agentsCount(int n) {
  agentsCount = n;
  createAgents();
}

class Agent {

  // current position
  float x, y;
  // previous position
  float px, py;
  // agent's shade (grey value)
  float shade; 
  float weight;
  float hue;
  float angle;


  // create agent that picks starting position itself
  Agent() {
    // default is all agent are at centre
    x = width / 2;
    y = height / 2;
    // random starting position
    //int m = 0; // margin
    //x = random(m, width - m);
    //y = random(m, height - m);
    // pick a random grey shade
    shade = 255 * PApplet.parseInt(random(1, 2));
    // pick random stroke weight
    weight = random(1, maxWeight);
    // pick a hue
    hue = (baseHue + random(0, 60)) % 360;
    angle = PApplet.parseInt(random(-180, 180));
   
  }

  // create agent at specific starting position
  Agent(float _x, float _y) {
    x = _x;
    y = _y;
  }

  public void update() {
    // save last position
    px = x;
    py = y;

    // pick a new position
    // pick a new position
    x = x + random(-maxStepSize, maxStepSize);
    y = y + random(-maxStepSize, maxStepSize);
    //x = x + maxStepSize * cos(angle);
    //y = y + maxStepSize * sin(angle);
    //if (x <= 0 || x >= width){
    //  println("x hit");
    //  angle = 90 + angle;
    //}
    //if (y <= 0 || y >= height){
    //  println("y hit");
    //  angle = 360-2*angle;
    //}
  }

  public void draw() {
    // draw a line between last position
    // and current position
    stroke(hue, 100, shade, opacity);
    strokeWeight(weight);
    //stroke(shade, opacity);
    line(px, py, x, y);
  }
}
/* 
 * Simple GUI for parameter adjustment
 */






class Gui {

  ControlP5 controlP5;

  ControlGroup ctrl;
  boolean showGUI = false;

  Gui(processing.core.PApplet sketch) {

    int activeColor = color(0, 130, 164);
    controlP5 = new ControlP5(sketch);
    //controlP5.setAutoDraw(false);
    controlP5.setColorActive(activeColor);
    controlP5.setColorBackground(color(170));
    controlP5.setColorForeground(color(50));
    controlP5.setColorCaptionLabel(color(50));
    controlP5.setColorValueLabel(color(255));

    ctrl = controlP5.addGroup("menu", left, 15, 35);
    ctrl.setColorLabel(color(255));

    // position of controls relative to menu toggle
    lastLine = 10;
    ctrl.close();
  }

  int lastLine;

  int left = 5;
  int lineHeight = 15;
  int padding = 5;

  public void newLine() {
    lastLine += lineHeight + padding;
  }

  public void addSpace() {
    lastLine += lineHeight/2 + padding;
  }

  public void addSlider(String variableName, float minVal, float maxVal) {
    Slider s = controlP5.addSlider(variableName, minVal, maxVal, left, lastLine, 300, lineHeight);

    s.setGroup(ctrl);
    //s.getCaptionLabel().toUpperCase(true);
    s.getCaptionLabel().getStyle().padding(4, 3, 3, 3);
    s.getCaptionLabel().getStyle().marginTop = -4;
    s.getCaptionLabel().getStyle().marginLeft = 0;
    s.getCaptionLabel().getStyle().marginRight = -14;
    s.getCaptionLabel().setColorBackground(0x99ffffff);

    newLine();
  }

  public void draw() {
    controlP5.show();
    controlP5.draw();
  }


  public void keyPressed() {

    if (key=='m' || key=='M') {
      showGUI = controlP5.getGroup("menu").isOpen();
      showGUI = !showGUI;
    }
    if (showGUI) { 
      controlP5.getGroup("menu").open();
      controlP5.getGroup("menu").show();
    } else {
      controlP5.getGroup("menu").close();
      if (key == 'M') 
        controlP5.getGroup("menu").hide();
    }
    
    // allow saving the frame too
    if (key=='s' || key=='S') {
      String t = String.format("%1$ty%1$tm%1$td_%1$tH%1$tM%1$tS", Calendar.getInstance()); 
      String fn = t + ".png";
      println("saving frame as '" + fn + "'");
      saveFrame(fn);
    }
    
  }
}
  public void settings() {  fullScreen(); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "--present", "--window-color=#666666", "--stop-color=#cccccc", "agentstarter" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
