
class Agent {

  // current position
  float x, y;
  // previous position
  float px, py;
  // agent's shade (grey value)
  float shade; 
  float weight;
  float hue;
  float angle;


  // create agent that picks starting position itself
  Agent() {
    // default is all agent are at centre
    x = width / 2;
    y = height / 2;
    // random starting position
    //int m = 0; // margin
    //x = random(m, width - m);
    //y = random(m, height - m);
    // pick a random grey shade
    shade = 255 * int(random(1, 2));
    // pick random stroke weight
    weight = random(1, maxWeight);
    // pick a hue
    hue = (baseHue + random(0, 60)) % 360;
    angle = int(random(-180, 180));
   
  }

  // create agent at specific starting position
  Agent(float _x, float _y) {
    x = _x;
    y = _y;
  }

  void update() {
    // save last position
    px = x;
    py = y;

    // pick a new position
    // pick a new position
    x = x + random(-maxStepSize, maxStepSize);
    y = y + random(-maxStepSize, maxStepSize);
    //x = x + maxStepSize * cos(angle);
    //y = y + maxStepSize * sin(angle);
    //if (x <= 0 || x >= width){
    //  println("x hit");
    //  angle = 90 + angle;
    //}
    //if (y <= 0 || y >= height){
    //  println("y hit");
    //  angle = 360-2*angle;
    //}
  }

  void draw() {
    // draw a line between last position
    // and current position
    stroke(hue, 100, shade, opacity);
    strokeWeight(weight);
    //stroke(shade, opacity);
    line(px, py, x, y);
  }
}