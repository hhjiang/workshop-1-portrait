
class Agent {

  // local shape transforms
  float scale = 1;
  float curScale = 0.1;
  boolean growing = true;
  float stepSize = random(1, 3);
  float noiseAngle;
  float alpha = 1;
  String state = "petal";
  

  // location of agent centre and shape to display
  float orig_x;
  float orig_y;
  float x;
  float y;
  float angle;
  float maxScale; //how large the petal will eventually grow to
  float growVelocity; //how fast the petal grows
  PShape shape;
  int time; //the time at which the agent was born
  Boolean isLeaf; //true if agent is a leaf (false if it's a petal or seed)
  Boolean isDead;
  
  // create the agent
  Agent(float _x, float _y, float _angle, float _maxScale, PShape _shape, int _time, Boolean _isLeaf, Boolean _isDead) {
    orig_x = _x;
    orig_y = _y;
    
    x = _x;
    y = _y;
    angle = _angle;
    maxScale = _maxScale;
    shape = _shape;
    time = _time;
    isLeaf = _isLeaf;
    isDead = _isDead;
  }

  void update() {
    int m = millis();
    int leafTime = 5;
    int petalTime = leafTime + 15;// //number of seconds before petals start closing
    int seedTime = petalTime + 10; //10; //number of seconds before seed state starts
    int blowTime = seedTime + 15; //15; //number of seconds before seeds start propogating
    int killTime = blowTime + 15; //number of seconds before agents are deleted
    
    if (m-time >= seedTime * 1000){
      state = "seed";
    }
    if (m-time >= blowTime * 1000){ //wait few seconds before the seeds start propogating
      state = "blow";
    }
    if (m-time >= killTime * 1000){ //wait few seconds before deleting the seeds
      state = "kill";
    }
    
    
    if (state != "blow"){
      angle += random(-350*liveInput.left.level(),350*liveInput.left.level()); //makes the petals 'move'
    }
    if (state == "kill"){ //kill off seeds
        isDead = true;
      }
    
    if (isLeaf) {
      if ((curScale < maxScale) && growing){
        Ani.to(this, random(6,8), "curScale", maxScale+0.6, Ani.QUAD_OUT);
      }
      else if (state == "blow"){ //leaves close up during blowing stage
        Ani.to(this, random(5,7), "curScale", 0, Ani.QUAD_OUT);
      }
    }
    
    else{
      if ((curScale < maxScale) && growing && m-time >= leafTime * 1000){
        Ani.to(this, random(6,8), "curScale", maxScale+0.6, Ani.QUAD_OUT);
      }
      if (m-time >= petalTime * 1000 && state=="petal"){ //close the flower
        Ani.to(this, random(5,7), "curScale", 0, Ani.QUAD_OUT);
        growing = false;
      }
      if (state == "seed"){ //produce seeds
        shape = loadShape("seed.svg"); //change to seed
        growing = true;
      }
      if (state == "blow"){ //seeds start propogating
          noiseAngle = noise(x+random(-10,10)/noiseScale, y+random(-10,10)/noiseScale) * noiseStrength;
          x += cos(noiseAngle) * stepSize;// * liveInput.left.level();
          y += sin(noiseAngle) * stepSize;// * liveInput.left.level();
          angle += noiseAngle;///stepSize;
          Ani.to(this, random(8,12), "curScale", 0, Ani.QUAD_OUT);
      }
    }
}

  void draw() {
    pushMatrix();
    translate(x, y);
    rotate(radians(angle));
    noStroke();
    noFill();    
    shapeMode(CENTER);
   
    //fill(colour);
    float s = curScale*20;
    shape(shape, 0, 0, s, s);
    popMatrix();
  }
}