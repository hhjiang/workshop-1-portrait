/* Helga Jiang (20578811)
 * CS 383: Assignment 2
 * Dandelion Thought Machine
 *
 *
 */

Gui gui;
import processing.video.*;
float fullScreenScale;
boolean fullScreen;
import gab.opencv.*;
// to get Java Rectangle type
import java.awt.*; 

Capture cam; //grabs frame and passes it into the openCV object
OpenCV opencv; //way to access the opencv methods
// scale factor to downsample frame for processing 
float scale = 0.1; //affects frame rate & the pixelation
// image to display
PImage output;

// array of bounding boxes for face
Rectangle[] faces;
int[] faceTimes; //keeps track of how long the face has been there, within ~10 px change
int[] facePositions; //keeps track of the previous facex position per face. facePos[0] represents the first face's previous center x position


import de.looksgood.ani.*;
import de.looksgood.ani.easing.*;

import ddf.minim.*;
import ddf.minim.analysis.*;

Minim minim;
AudioInput liveInput; //live mic input

float noiseScale = 400;
float noiseStrength = 10; 

// global variables for agents
int leavesCount = 0; //10;
int petalsCount = 80;
int agentsCount; //total number of agents per dandelion
// list of agents
ArrayList<Agent> agents;
int numLiveDandelions = 0; //keeps track of the number of dandelions in the frame

//String state = "petal";
int numFaces = 0;

void setup() {
  //size(640, 480);
  //fullScreen = false;
  
  fullScreen();
  fullScreen = true;
  
  // assumes same aspect ratio, otherwise pick if you want w or h to define size
  fullScreenScale = width / float(640);

  minim = new Minim(this); //pass this to Minim so that it can load files from the data directory
  liveInput = minim.getLineIn(); // use the getLineIn method of the Minim object to get an AudioInput

  // want video frame and opencv proccessing to same size
  cam = new Capture(this, int(640 * scale), int(480 * scale));

  opencv = new OpenCV(this, cam.width, cam.height);
  opencv.loadCascade(OpenCV.CASCADE_FRONTALFACE);  //can detect other objects too, like mouth

  cam.start();

  // init to empty image
  output = new PImage(cam.width, cam.height); //initialize output image

  // you have to call always Ani.init() first!
  Ani.init(this);
  
  agents = new ArrayList<Agent>();
  faceTimes = new int[20]; 
  facePositions = new int[20]; 
  agentsCount = leavesCount + petalsCount;
}

void createAgents(int x, int y) {  
  int time = millis();
  // load the SVG shape 
  PShape petal = loadShape("petal.svg");
  PShape leaf = loadShape("leaf.svg");  
  int angle;
  float maxScale;
  
  for (int i = 0; i < leavesCount; i++) { //create leaf agents
    angle = i*(360/leavesCount);
    maxScale = random(0.2,0.6);
    Agent a = new Agent(x, y, angle, maxScale, leaf, time, true, false);
    agents.add(a);
  }
  
  int initPetals = 15; //original starting petals (add from the back) so there is an even circle
  for (int i = 0; i < petalsCount; i++) {
    if (i<initPetals) {
      angle = i*(360/initPetals);
      maxScale = random(0.05,0.5);
    }
    else {
      angle = int(random(i*30, i*60));
      maxScale = random(1/(i+1),1);
    }
    Agent a = new Agent(x, y, angle, maxScale, petal, time, false, false);
    agents.add(a);
  }
}

void draw() {
  background(0);
  
  if (cam.available() == true) {
    cam.read();

    // load frame into OpenCV 
    opencv.loadImage(cam);

    // it's often useful to mirror image to make interaction easier
    // 1 = mirror image along x
    // 0 = mirror image along y
    // -1 = mirror x and y
    opencv.flip(1);

    faces = opencv.detect();

    // switch to RGB mode before we grab the image to display
    opencv.useColor(RGB);
    output = opencv.getSnapshot(); 
  }
  
  // draw the image and graphics at full scale resolution
  if (fullScreen) { 
    scale(fullScreenScale, fullScreenScale);
  } 

  // draw the camera image
  //pushMatrix();
  //scale(1 / scale);
  //tint(255, 80); // partially transparent camera image
  //image(output, 0, 0 );
  //popMatrix();


  // draw face tracking debug
  if (faces != null) {
    for (int i = 0; i < faces.length; i++) {
      
      // scale the tracked faces to canvas size
      float s = 1 / scale;
      int facex = int(faces[i].x * s);
      int facey = int(faces[i].y * s);
      int facew = int(faces[i].width * s);
      int faceh = int(faces[i].height * s);
      Boolean dandelionInArea = false;
      int radius = 150; //used to check if face is within # pixels radius
      int center_x = facex + facew/2;
      int center_y = facey + faceh/2;
      
      //for (Agent a : agents) {
      for (int j=0; j<numLiveDandelions; j++){
        println(dandelionInArea);
        //if face is within radius pixels of existing dandelion
        if (sq(center_x - agents.get(j*agentsCount).orig_x) + sq(center_y - agents.get(j*agentsCount).orig_y) < sq(radius)){
          println("center: " + center_x + ", " + center_y + " dand: " + agents.get(j*agentsCount).orig_x + ", " + agents.get(j*agentsCount).orig_y);
          dandelionInArea = true;
          break;
        }
      }
      if (!dandelionInArea && i >= numLiveDandelions){
        createAgents(facex + facew/2, facey + faceh/2);
      }
      
      // draw bounding box and a "face id"
      //stroke(255, 255, 0);
      //noFill();     
      //rect(facex, facey, facew, faceh);
      //fill(255, 255, 0);
      //text(i, facex, facey - 20);
      
    }
  }

  // draw all the agents
  for (Agent a : agents) {
    a.update();
    a.draw();
  }
  for (int i = 0; i<agents.size(); i++){
    if (agents.get(i).isDead){
      agents.remove(i);
    }
  }
  numLiveDandelions = agents.size()/agentsCount;
  println("agents.size(): " + agents.size());
  println("numLiveDandelions: " + numLiveDandelions);
}

// enables shortcut keys for the Gui
// 'm' or 'M' to show menu, 'm' to partially hide, 'M' to fully hide
void keyPressed() {
  //reset dandelions if key pressed
  for (int i = agents.size() - 1; i >= 0; i--) { 
    agents.remove(i);
  }
  numLiveDandelions = 0;
}