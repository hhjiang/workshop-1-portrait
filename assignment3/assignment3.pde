// Code adapted from Daniel Shiffman's depth thresholding example
// https://github.com/shiffman/OpenKinect-for-Processing
// http://shiffman.net/p5/kinect/

//try multiple people by counting number of circles (don't use SimpleOpenNI

import org.openkinect.freenect.*;
import org.openkinect.processing.*;

Kinect kinect;

// Depth image
PImage depthImg;

// depth pixels
int minDepth =  60;
int maxDepth = 950;

int manualX = 720;
int manualY = 740;

// kinect's angle
float angle;

// emoticon svgs
PShape smiley;
PShape frowny;
String currentShape = "frowny";

int[] shapeSizes;
int skip; // = 50; // the number of pixels between each emoticon

float fullScreenScale;
boolean fullScreen;

void setup() {
  //size(640, 480);
  //fullScreen = false;
  
  fullScreen();
  fullScreen = true;
  
  // assumes same aspect ratio, otherwise pick if you want w or h to define size
  fullScreenScale = width / float(640);

  kinect = new Kinect(this);
  kinect.initDepth();
  angle = kinect.getTilt();

  // Blank image
  depthImg = new PImage(kinect.width, kinect.height);
  
  // load emoticon svgs
 // emoticons = loadEmoticons("emoticons/", 1);
  smiley = loadShape("emoticons/1.svg");
  frowny = loadShape("emoticons/2.svg");
  
  //determine array for the sizes of the smileys
  shapeSizes = new int[500];
  for (int i=0; i<shapeSizes.length; i++){
    shapeSizes[i] = int(random(20,30));
  }
  skip = 35; // the number of pixels between each emoticon
}


void draw() {
  background(0);
  // Draw the raw image
  //image(kinect.getDepthImage(), 0, 0);

  // get the Depth Image
  PImage img = kinect.getDepthImage();

  // Threshold the depth image by taking the raw depth data
  int[] rawDepth = kinect.getRawDepth();
  int prevDetectedIndex = 0;
  
  
  //int shapeSize = 30; // avg size of smiley
  //int shapeFactor = 10; //amount of variation in shapeSize, random
  ArrayList<Integer> points = new ArrayList<Integer>();
  
  for (int y = 0; y < img.height; y+=skip) {
    for (int x = 0; x < img.width; x++) {
      int index = x + y * img.width;
      if (rawDepth[index] >= minDepth && rawDepth[index] <= maxDepth) {
        // make sure we only draw an emoticon  when it doesn't collide with others
        if (index >= prevDetectedIndex + skip) {
          //depthImg.pixels[index] = color(255);
          
          prevDetectedIndex = index;
          
          //keep track of all the existing indexes where an emoticon should exist
          points.add(index);
        }
        else { //otherwise, draw black
          depthImg.pixels[index] = color(0);
        }
      } else { //draw black if there is no object within the depth threshold
        depthImg.pixels[index] = color(0);
      }
    }
  }

  // Draw the thresholded image
  depthImg.updatePixels();
  //image(depthImg, 0, 0);
  
  if (points.size() > 60){ //if there's more than one person in the screen
    currentShape = "smiley";
  }
  else{
    currentShape = "frowny";
  }
  println(currentShape);
  
  // Draw the emoticons in the given points
  int counter = 0;
  for (Integer point : points){
    int xpos = point%img.width;
    int ypos = point/img.width + 1;
    if (currentShape == "smiley"){
      shape(smiley, 
            xpos + manualX, ypos + manualY, 
            shapeSizes[counter], shapeSizes[counter]);
    }
    else {
      shape(frowny, 
            xpos + manualX, ypos + manualY, 
            shapeSizes[counter], shapeSizes[counter]);
    }
    counter+=1;
  }

  fill(255);
 // text("TILT: " + angle, 10, 20);
 // text("THRESHOLD: [" + minDepth + ", " + maxDepth + "]", 10, 36);
  
  
  // draw the image and graphics at full scale resolution
  if (fullScreen) { 
    scale(fullScreenScale, fullScreenScale);
  } 
}

// Adjust the angle and the depth threshold min and max
void keyPressed() {
  // Manual adjust where projection displays
  if (key == CODED) {
    if (keyCode == UP) {
      manualY-=10;
    } else if (keyCode == DOWN) {
      manualY+=10;
    } else if (keyCode == LEFT) {
      manualX-=10;
    } else if (keyCode == RIGHT) {
      manualX+=10;
    }
  } 
  
  else if (key == 'a') {
    minDepth = constrain(minDepth+10, 0, maxDepth);
  } else if (key == 's') {
    minDepth = constrain(minDepth-10, 0, maxDepth);
  } else if (key == 'z') {
    maxDepth = constrain(maxDepth+10, minDepth, 2047);
  } else if (key =='x') {
    maxDepth = constrain(maxDepth-10, minDepth, 2047);
  } 
  
  // Tilt the Kinect
  else if (key =='1') {
    angle--;
  } else if (key =='2') {
    angle++;
  }
  angle = constrain(angle, 0, 30);
  kinect.setTilt(angle);
  
  println(manualX, manualY);
  
}

// load in the images
PShape[] loadEmoticons(String path, int n) {

  PShape[] e = new PShape[n];

  for (int i = 0; i < n; i++) {
    e[i] = loadShape(path + (i+1) + ".svg");
  } 
  return e;
}
