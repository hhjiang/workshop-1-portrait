/* 
 * Face tracking
 * 
 *    Code based on demos in OpenCV for Processing 0.5.4 
 *    by Greg Borenstein http://gregborenstein.com
 */
 
 /* CS/FINE 383: Assignment 1
*  Helga Jiang (20578811)
*/

import processing.video.*;

import gab.opencv.*;

// to get Java Rectangle type
import java.awt.*; 

import ddf.minim.*;
import ddf.minim.analysis.*;
 
Minim minim;
//AudioPlayer song; //load sound file into song
AudioInput liveInput; //live mic input
//AudioMetaData meta; //An object filled with metadata about a file, such as ID3 tags.
BeatDetect beat; //beat detection


Capture cam; //grabs frame and passes it into the openCV object
OpenCV opencv; //way to access the opencv methods

// scale factor to downsample frame for processing 
float scale = 0.5; //affects frame rate & the pixelation

// image to display
PImage output;
PImage faceSwap;

PImage[] filters;
int filterNum = 2; // keeps track of the order of filters shown (press space bar to loop through / change filters)

// array of bounding boxes for face
Rectangle[] faces;


//bubble boxes
final static ArrayList<Box> boxes = new ArrayList();
float r = 0; //
int timer;
float co = 100;

//particles
final static ArrayList<Particle> particles = new ArrayList();   // Initialize the arraylist

//debugging flag
boolean debug = false;


void setup() {
  //fullScreen();
  //fullScreen = true;
  size(640, 480, P3D);
  //fill(0, 0, 0, .5);
  
  minim = new Minim(this); //pass this to Minim so that it can load files from the data directory
  //song = minim.loadFile("song.mp3"); //load in the song file
  liveInput = minim.getLineIn(); // use the getLineIn method of the Minim object to get an AudioInput
  beat = new BeatDetect(); //http://code.compartmental.net/minim/beatdetect_class_beatdetect.html
 // song.loop();   
  
  // want video frame and opencv proccessing to same size
  cam = new Capture(this, int(640 * scale), int(480 * scale));

  opencv = new OpenCV(this, cam.width, cam.height);
  opencv.loadCascade(OpenCV.CASCADE_FRONTALFACE);  //can detect other objects too, like mouth

  cam.start();

  // init to empty image
  output = new PImage(cam.width, cam.height); //initialize output image
}


void draw() {
  
  if (cam.available() == true) {
    cam.read();

    // load frame into OpenCV 
    opencv.loadImage(cam);

    // it's often useful to mirror image to make interaction easier
    // 1 = mirror image along x
    // 0 = mirror image along y
    // -1 = mirror x and y
    opencv.flip(1);

    faces = opencv.detect();

    // switch to RGB mode before we grab the image to display
    opencv.useColor(RGB);
    output = opencv.getSnapshot(); //takes current version of image inside openCV
  }
  fill(0, 0, 0, .5);


   
  float angle = 0;
  
  // draw the image
  pushMatrix();
  scale(1 / scale);
  image(output, 0, 0 );
  popMatrix();
  
  stroke(255,255,255);

  // draw face tracking debug
  if (faces != null) {
    for (int i = 0; i < faces.length; i++) {
      
      // scale the tracked faces to canvas size
      float s = 1 / scale;
      int facex = int(faces[i].x * s);
      int facey = int(faces[i].y * s);
      int facew = int(faces[i].width * s);
      int faceh = int(faces[i].height * s);
      
      // FIRST FILTER: WAVES
      if (i == 1) { // first face in frame
        float numWaves = 15; // number of waves in filter
        for (int k = 0; k < numWaves; k++) { //draw the waves
          //rotate(angle); 
          //angle += 0.03;
          for(int j = 0; j < liveInput.bufferSize() - 1; j++) {
            //angle += 0.03;
            float x1 = map( j, 0, liveInput.bufferSize(), 0, faceh/2 );
            float x2 = map( j+1, 0, liveInput.bufferSize(), 0, faceh/2 );
  
            float xDiff = facex + k*(facew/numWaves); //position each wave within the x-axis
            float yDiff;
            if (k > numWaves/2) {
              //yDiff = facey + k*10;
              yDiff = facey - (numWaves-k)*(faceh/numWaves);
            }
            else {
              yDiff = facey - k*(faceh/numWaves);
            }
            //float yDiff = facey + yOvalDiff*k*10;
            
            float lineWidth = 250; //change this number to change the width (how dramatic) the waves are 
            
            line( xDiff + liveInput.left.get(j)*lineWidth, yDiff-x1, xDiff + liveInput.left.get(j+1)*lineWidth, yDiff-x2 );
            
            //line( x1, 150 + song.right.get(j)*50, x2, 150 + song.right.get(j+1)*50 );
            //line( x1, 50 + song.left.get(j)*50, x2, 50 + song.left.get(j+1)*50 );
            //line( x1, 150 + song.right.get(j)*50, x2, 150 + song.right.get(j+1)*50 );
          }
        }
      }
      
      // SECOND FILTER: box bubbles
      if (i == 1){ // second face in frame
        stroke(255,255,255);
        noFill();
        //boxes
        for(Box box: boxes){
          box.draw_box();
        }
        r=r+0.01; //rotating metric
        
        //amplitude / buffer levels
        // the value returned by the level method is the RMS (root-mean-square) 
        // value of the current buffer of audio.
        // see: http://en.wikipedia.org/wiki/Root_mean_square
        //rect( 0, 0, song.left.level()*width, 100 );
        //rect( 0, 100, song.right.level()*width, 100 );
        
         if (millis() - timer >= 100) {
          for (int j=0; j<liveInput.left.level()*10; j++){
            println(liveInput.left.level());
            boxes.add( new Box(facex, facey, 6, r*0.5, angle));
            angle += 1;
          }
          timer = millis();
        } 
      }
      // THIRD FILTER: particles
      // adapted from Daniel Shiffman's Multiple Particle System: https://processing.org/examples/multipleparticlesystems.html
      if (i == 0) {
        particles.add(new Particle(new PVector(facex + facew/2, facey)));
        for (int j = particles.size()-1; j >= 0; j--) {
          Particle p = particles.get(j);
          p.run();
          if (p.isDead()) {
            particles.remove(j);
          }
        }
      }
      
    }
  }
  fill(255, 0, 0);
  text(nfc(frameRate, 1), 20, 20);
}

void  keyPressed(){
}


//box class adapted from: https://stackoverflow.com/questions/27437796/mousepressed-creates-3d-cube-in-processing
class Box{
  float x,y,z, box_size, rot, angle;

  //create instance of box with specific position and starting rotation
  Box(float x, float y, float box_size, float rot, float angle){
    this.z = box_size;
    this.x = x;
    this.y = y;
    this.box_size = box_size;
    this.rot = rot;
    this.angle = angle;
  }

  void draw_box(){
    pushMatrix();
    translate(x, y, z);
    //adding R will rotate all boxes
    x += cos(angle); //change position of box so it moves outwards from mouse position
    y += sin(angle);
    rotateX(r + rot);
    rotateY(r + rot);
    fill(map(co, 0, 0, 0, 0));
    box(box_size);
    popMatrix();
  }

  //here you can add more complex methods like colision ...
}

// A simple Particle class
class Particle {
  PVector position;
  PVector velocity;
  PVector acceleration;
  float lifespan;

  Particle(PVector l) {
    acceleration = new PVector(0, 0.05);
    velocity = new PVector(random(-1, 1), random(-2, 0));
    position = l.copy();
    lifespan = 305.0;
  }

  void run() {
    update();
    display();
  }

  // Method to update position
  void update() {
    velocity.add(acceleration);
    position.add(velocity);
    lifespan -= 5.0;
  }

  // Method to display
  void display() {
    stroke(255, lifespan);
    fill(255, lifespan);
    ellipse(position.x, position.y, liveInput.left.level()*250, liveInput.left.level()*250);
  }

  // Is the particle still useful?
  boolean isDead() {
    return (lifespan < 0.0);
  }
}
 