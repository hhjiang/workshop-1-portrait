/* CS/FINE 383: Assignment 1
*  Helga Jiang (20578811)
*/

import ddf.minim.*;
import ddf.minim.analysis.*;
 
Minim minim;
AudioPlayer song; //load sound file into song
//AudioMetaData meta; //An object filled with metadata about a file, such as ID3 tags.
BeatDetect beat; //beat detection

float eRadius;
float r=0;
float co = 100;
int timer;

void setup(){
  size(500,500,P3D);

  //pass this to Minim so that it can load files from the data directory
  minim = new Minim(this);
  song = minim.loadFile("song.mp3"); //load in the song file
  //meta = player.getMetaData();
  beat = new BeatDetect(); //http://code.compartmental.net/minim/beatdetect_class_beatdetect.html
  song.loop(); 
  
  ellipseMode(RADIUS);
  eRadius = 20;
}

final static ArrayList<Box> boxes = new ArrayList();
 
void draw(){
  background(0);
  //stroke(255,255,255);
  //noFill();
  
  
  // draw the waveforms
  // the values returned by left.get() and right.get() will be between -1 and 1,
  // so we need to scale them up to see the waveform
  // note that if the file is MONO, left.get() and right.get() will return the same value
  // adapted from Minim doc examples: http://code.compartmental.net/minim/audioplayer_field_mix.html
  for(int i = 0; i < song.bufferSize() - 1; i++)
  {
    float x1 = map( i, 0, song.bufferSize(), 0, width );
    float x2 = map( i+1, 0, song.bufferSize(), 0, width );
    line( x1, 50 + song.left.get(i)*50, x2, 50 + song.left.get(i+1)*50 );
    line( x1, 150 + song.right.get(i)*50, x2, 150 + song.right.get(i+1)*50 );
  }
  
  // draw a line to show where in the song playback is currently located
  float posx = map(song.position(), 0, song.length(), 0, width);
  stroke(0,200,0);
  line(posx, 0, posx, height);
  
  if ( song.isPlaying() )
  {
    text("Press any key to pause playback.", 10, 20 );
  }
  else
  {
    text("Press any key to start playback.", 10, 20 );
  }
  
  //beat visual
  beat.detect(song.mix);
  float a = map(eRadius, 20, 80, 60, 255);
  stroke(255,255,255);
  fill(255, 255, 255, a);
  if ( beat.isOnset() ) eRadius = 80;
  ellipse(mouseX, mouseY, eRadius, eRadius);
  eRadius *= 0.95;
  //if ( eRadius < 20 ) eRadius = 20;
  
  //boxes
  for(Box box: boxes){
    box.draw_box();
  }
  r=r+0.01; //rotating metric
  
  //amplitude / buffer levels
  // the value returned by the level method is the RMS (root-mean-square) 
  // value of the current buffer of audio.
  // see: http://en.wikipedia.org/wiki/Root_mean_square
  //rect( 0, 0, song.left.level()*width, 100 );
  //rect( 0, 100, song.right.level()*width, 100 );
  
   if (millis() - timer >= 100) {
    for (int i=0; i<song.left.level()*10; i++){
      println(song.left.level());
      boxes.add( new Box(mouseX, mouseY, 6, r*0.5, angle));
      angle += 1;
    }
    timer = millis();
  } 
}

float angle = 0;
//void mousePressed() {
//  boxes.add( new Box(mouseX, mouseY, 10, r*0.5, angle));
//  angle += 1;
//}

void keyPressed()
{
  if ( song.isPlaying() )
  {
    song.pause();
  }
  // if the player is at the end of the file,
  // we have to rewind it before telling it to play again
  else if ( song.position() == song.length() )
  {
    song.rewind();
    song.play();
  }
  else
  {
    song.play();
  }
}

//box class adapted from: https://stackoverflow.com/questions/27437796/mousepressed-creates-3d-cube-in-processing
class Box{
  float x,y,z, box_size, rot, angle;

  //create instance of box with specific position and starting rotation
  Box(float x, float y, float box_size, float rot, float angle){
    this.z = box_size;
    this.x = x;
    this.y = y;
    this.box_size = box_size;
    this.rot = rot;
    this.angle = angle;
  }

  void draw_box(){
    pushMatrix();
    translate(x, y, z);
    //adding R will rotate all boxes
    x += cos(angle); //change position of box so it moves outwards from mouse position
    y += sin(angle);
    rotateX(r + rot);
    rotateY(r + rot);
    fill(map(co, 0, 0, 0, 0));
    box(box_size);
    popMatrix();
  }

  //here you can add more complex methods like colision ...
}
 